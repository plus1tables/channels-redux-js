import React from 'react';


class PatientComponent extends React.Component {
    constructor(props) {
        super(props);

        /* This is all to replace the render() function with a new render function
         * that respects the isReady() function. It's important to bind the original render
         * function to `this` so that it can have access to the object just like the original
         * render function.
         */
        this.wrapRender = this.wrapRender.bind(this);
        this._render = this.render.bind(this);
        this.render = this.wrapRender(this._render).bind(this);
    }

    isReady() {
        return true;
    }

    defaultContents() {
        return <div className="spinner-wrapper">
            <div className="default-loading-spinner"/>
        </div>;
    }

    wrapRender(render) {
        return () => this.isReady() ? render() : this.defaultContents();
    }
}

export default PatientComponent;
