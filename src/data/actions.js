import request from "./request";

export const CREATE_OBJECT = 'CREATE_OBJECT';
export function createObject(model, pk, object) {
    return {
        type: CREATE_OBJECT,
        model,
        pk,
        object
    };
}

export const UPDATE_OBJECT = 'UPDATE_OBJECT';
export function updateObject(model, pk, object) {
    return {
        type: UPDATE_OBJECT,
        model,
        pk,
        object
    };
}

export const PATCH_OBJECT = 'PATCH_OBJECT';
export function partialUpdateObject(model, pk, updatedFields) {
    return {
        type: PATCH_OBJECT,
        model,
        pk,
        updatedFields
    };
}

export const DELETE_OBJECT = 'DELETE_OBJECT';
export function deleteObject(model, pk) {
    return {
        type: DELETE_OBJECT,
        model,
        pk
    };
}

export const REQUEST_STARTED = 'REQUEST_STARTED';
function requestStarted(url) {
    return {
        type: REQUEST_STARTED,
        url
    }
}

export const REQUEST_FINISHED = 'REQUEST_FINISHED';
function requestFinished(url) {
    return {
        type: REQUEST_FINISHED,
        url
    }
}

function ensureHost(url) {
    if (url.startsWith(window.location.origin)) {
        return url; // Has host
    }
    let joiner = '/';
    if (url.startsWith('/')) {
        joiner = '';
    }
    return window.location.origin + joiner + url;
}

export function receivedNotification(message) {
    return function (dispatch) {
        if (message.type === "deleted") {
            let key = message[window.key_field];
            if (window.key_field === 'url') {
                key = ensureHost(key);
            }
            return dispatch(deleteObject(message.model, key));
        }

        dispatch(requestStarted(message.url));
        return request().get(message.url)
                    .then((apiResponse) => {
                        switch (message.type) {
                            case "created":
                                dispatch(createObject(message.model, apiResponse.data[window.key_field], apiResponse.data));
                                break;
                            case "updated":
                                dispatch(updateObject(message.model, apiResponse.data[window.key_field], apiResponse.data));
                                break;
                        }
                    })
                    .then(() => dispatch(requestFinished(message.model, message.pk)));
    }
}

export function fetchObject(model, url) {
    return function (dispatch) {
        dispatch(requestStarted(url));
        return request().get(url)
                        .then((apiResponse) => {
                            dispatch(createObject(model, apiResponse.data[window.key_field], apiResponse.data));
                            dispatch(requestFinished(url));
                        });
    }
}

export function patchObject(model, url, updatedFields) {
    return function (dispatch) {
        dispatch(partialUpdateObject(model, url, updatedFields));
        return request().patch(url, updatedFields);
    }
}
