import reducers from './reducers'
import {applyMiddleware, combineReducers, compose, createStore as reduxCreateStore} from 'redux'
import thunkMiddleware from 'redux-thunk'
import {fetchObject} from "./actions";


class InnerFetchHandler {
    constructor(getState, dispatch, model) {
        this.getState = getState;
        this.dispatch = dispatch;
        this.get = this.get.bind(this);
        this.model = model;
    }

    static validUrl(str) {
        try {
            return str.startsWith('http');
        } catch (e) {
            return false;
        }
    }

    get(target, name) {
        if (name === '__isProxy') {
            return true;
        }

        let object = target[name];
        if (object === undefined && InnerFetchHandler.validUrl(name)) {
            if (!this.getState().requestStatus.hasOwnProperty(name)) {
                this.dispatch(fetchObject(this.model, name));
            }
            return null;
        }
        return object;
    }
}

class FetchHandler {
    constructor(getState, dispatch) {
        this.getState = getState;
        this.dispatch = dispatch;
        this.get = this.get.bind(this);
    }

    get(target, name) {
        if (name === '__isProxy') {
            return true;
        }

        let model = target[name];
        if (model === undefined) {
            model = {};
        }
        const innerFetchHandler = new InnerFetchHandler(this.getState, this.dispatch, name);
        return new Proxy(model, innerFetchHandler);
    }
}

function applyStateFetchHandler(store) {
    const originalGetState = store.getState;
    store.getState = function () {
        const state = originalGetState();
        if (!state.objects.__isProxy) {
            const fetchHandler = new FetchHandler(originalGetState, store.dispatch);
            state.objects = new Proxy(state.objects, fetchHandler);
        }
        return state;
    };
}

export default function createStore({extraReducers = {}, extraMiddleware = []} = {}) {
    const composeEnhancers = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

    let allMiddleware = [thunkMiddleware, ...extraMiddleware];
    let enhancer = composeEnhancers(applyMiddleware(...allMiddleware));

    let allReducers = reducers;
    if (extraReducers !== null) {
        allReducers = Object.assign(allReducers, extraReducers);
    }
    let rootReducer = combineReducers(allReducers);

    const store = reduxCreateStore(
        rootReducer,
        window.props,
        enhancer
    );

    applyStateFetchHandler(store);
    return store;
}
