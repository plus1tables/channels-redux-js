import {
    CREATE_OBJECT,
    DELETE_OBJECT,
    UPDATE_OBJECT,
    REQUEST_STARTED,
    REQUEST_FINISHED,
    PATCH_OBJECT
} from "./actions";

const objects = (state = {}, action) => {
    const newState = Object.assign({}, state);
    switch (action.type) {
        case UPDATE_OBJECT:
        case CREATE_OBJECT:
            if (!newState.hasOwnProperty(action.model)) {
                newState[action.model] = {};
            }
            newState[action.model][action.pk] = action.object;
            break;
        case PATCH_OBJECT:
            let existingObject = newState[action.model][action.pk];
            newState[action.model][action.pk] = Object.assign(existingObject, action.updatedFields);
            break;
        case DELETE_OBJECT:
            delete newState[action.model][action.pk];
            break;
        default:
            return state;
    }
    return newState;
};

const requestStatus = (state = {}, action) => {
    switch (action.type) {
        case REQUEST_STARTED:
            return Object.assign({}, state, {[action.url]: action.url});
        case REQUEST_FINISHED:
            let newState = Object.assign({}, state);
            delete newState[action.url];
            return newState;
        default:
            return state;
    }
};

export default {
    objects,
    requestStatus
};
