import {receivedNotification} from "./actions";

function addSubscriptions(target, model, normalizedIds) {
    let currentIds = target[model] || new Set();
    target[model] = new Set([...currentIds, ...normalizedIds]);
}

export default class WebSocketHandler {
    constructor(store, path='/ws/notify/') {
        this.receiveSocketMessage = this.receiveSocketMessage.bind(this);
        this.subscribeNew = this.subscribeNew.bind(this);
        this.subscribeExisting = this.subscribeExisting.bind(this);
        this.subscribeChanged = this.subscribeChanged.bind(this);
        this.subscribeAll = this.subscribeAll.bind(this);
        this.autoSubscribe = this.autoSubscribe.bind(this);
        this.stateChanged = this.stateChanged.bind(this);
        this.webSocketDisconnected = this.webSocketDisconnected.bind(this);
        this.webSocketConnected = this.webSocketConnected.bind(this);

        this.path = path;
        this.isOpen = false;
        this.subscribeNewQueue = [];
        this.subscribeExistingQueue = {};
        this.existingObjectSubscriptions = {};
        this.newObjectSubscriptions = [];
        this.store = store;
        this.currentObjectsState = null;
        this.reconnectTimeout = 5000;
        this.maxReconnectAttempts = 100;
        this.connectionAttempt = null;
        this.connectionFailureCount = 0;

        this.connect = this.connect.bind(this);
        this.connect();
    }

    connect() {
        this.subscribeNewQueue = this.subscribeNewQueue.concat(this.newObjectSubscriptions);
        Object.keys(this.existingObjectSubscriptions).forEach(model => {
            addSubscriptions(this.subscribeExistingQueue, model, this.existingObjectSubscriptions[model])
        });

        // If these don't get cleared they will get added to the queues on every connection failure.
        this.newObjectSubscriptions = [];
        this.existingObjectSubscriptions = {};
        
        let protocol = location.protocol === 'https:' ? 'wss://' : 'ws://';
        this.webSocket = new WebSocket(protocol + window.location.host + this.path);
        this.webSocket.onopen = this.webSocketConnected;
        this.webSocket.onmessage = this.receiveSocketMessage;
        this.webSocket.onclose = this.webSocketDisconnected;
    }

    webSocketConnected(response) {
        this.isOpen = true;
        if (this.connectionAttempt != null) {
            clearTimeout(this.connectionAttempt);
            this.connectionAttempt = null;
            this.connectionFailureCount = 0;
            console.info('Websocket reconnected');
        } else {
            console.info('Websocket Connected');
        }
        this.subscribeNewQueue.forEach(this.subscribeNew);
        this.subscribeNewQueue = [];

        Object.keys(this.subscribeExistingQueue).forEach(
            (model) => this.subscribeExisting(model, this.subscribeExistingQueue[model], true)
        );
        this.subscribeExistingQueue = {};
    }

    webSocketDisconnected(response) {
        console.error('WebSocket closed unexpectedly');
        this.isOpen = false;
        this.connectionAttempt = setTimeout(this.connect, this.reconnectTimeout);
        this.connectionFailureCount += 1;

        if (this.connectionFailureCount > this.maxReconnectAttempts) {
            clearTimeout(this.connectionAttempt);
            this.connectionAttempt = null;
            this.connectionFailureCount = 0;
            console.error('Reached maximum number of retries');
        }
    }

    receiveSocketMessage(message) {
        let data = JSON.parse(message.data);
        if (data.type === "created") {
            this.subscribeExisting(data.model, [data.pk])
        }
        this.store.dispatch(receivedNotification(data));
    }

    subscribeNew(model) {
        if (!this.isOpen) {
            this.subscribeNewQueue.push(model);
            return;
        }

        if (this.newObjectSubscriptions.includes(model)) {
            return; // Already subscribed
        }

        this.webSocket.send(JSON.stringify({
            'type': 'subscribe.new',
            model
        }));
        this.newObjectSubscriptions.push(model);
    }

    subscribeExisting(model, ids, normalized = false) {
        let normalizedIds;
        if (normalized) {
            normalizedIds = ids;
        } else {
            normalizedIds = WebSocketHandler.normalizeToIds(ids);
        }

        if (!this.isOpen) {
            addSubscriptions(this.subscribeExistingQueue, model, normalizedIds);
            return;
        }
        this.webSocket.send(JSON.stringify({
            'type': 'subscribe.existing',
            model,
            ids: [...normalizedIds] // Convert to array in case its a set
        }));

        addSubscriptions(this.existingObjectSubscriptions, model, normalizedIds);
    }

    subscribeAll() {
        let allObjects = this.store.getState().objects;

        Object.keys(allObjects).forEach((model) => {
            let objects = allObjects[model];
            let ids = Object.values(objects).map((obj) => obj.pk);

            this.subscribeNew(model);
            this.subscribeExisting(model, ids, true);
        });
    }

    autoSubscribe() {
        this.subscribeAll();
        let unsubscribe = this.store.subscribe(this.stateChanged);
        this.stateChanged();
        return unsubscribe;
    }

    stateChanged() {
        let nextState = this.store.getState().objects;
        if (nextState !== this.currentObjectsState) {
            this.currentObjectsState = nextState;
            Object.keys(this.currentObjectsState).forEach(this.subscribeChanged);
        }
    }

    subscribeChanged(model) {
        if (!this.newObjectSubscriptions.includes(model)) {
            this.subscribeNew(model);
        }

        let newIds = Object.values(this.currentObjectsState[model]).map((obj => obj.pk));
        let previousIds = this.existingObjectSubscriptions[model] || new Set();

        let changedIds = newIds.filter(id => !previousIds.has(id));
        if (changedIds.length > 0) {
            this.subscribeExisting(model, changedIds, true);
        }
    }

    static normalizeToIds(subscriptions) {
        if (Array.isArray(subscriptions)) {
            return subscriptions.map((obj) => {
                if (obj.pk === undefined) { // Subscriptions are an array of numbers
                    return obj;
                }
                return obj.pk;
            });
        }

        let objects = Object.values(subscriptions);

        let isSingleObject = false;
        let ids = objects.map((obj) => {
            let pk = obj.pk;
            if (pk === undefined) {
                isSingleObject = true;
            }
            return pk;
        });

        if (isSingleObject) {
            return [objects.pk];
        }
        return ids;
    }
}
