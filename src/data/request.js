import axios from "axios";
import Cookies from "js-cookie";


export default function request() {
    let csrftoken = Cookies.get('csrftoken');
    return axios.create({
        headers: {
            'X-CSRFToken': csrftoken
        }
    });
}
