# Channels-Redux

This is the frontend js companion to the pypi package also called [channels-redux](https://pypi.org/project/channels-redux/)

Channels-Redux.js provides a simple framework for subscribing to 
and receiving notifications about changes to the django data model.
Channels-Redux keeps your redux data store in sync with the database.

## Getting started

See Channels-Redux python for a quick start guide

## Exposed Modules

### createStore
This creates a redux store. Out of the box it comes hydrated with the data in window.props. If you use 
[django-react-views](https://pypi.org/project/django-react-views/)
on the server then window.props is provided for you.

For this most part this is a normal redux store, with redux-thunk's thunkMiddleware applied. And reducers made for
update the objects from window.props applied.

The initial redux state should look something like this:

```json
{
    "objects": {
        "django_app_name.model_name": {
            "https://example.com/api/app_name/model_name/1/": {
                "field_a": "value_a"
            }
        }
    },
    "requestStatus": "INACTIVE"
}
```
It is recommended that the objects are indexed by their rest api urls because it makes retrieving the objects easy. 
[django-react-views](https://pypi.org/project/django-react-views/) does this by default.

It takes an optional options object as an argument. The object has the following options:

`extraMiddleware`. This should be a list of any middleware you want to include in your store.

`extraReducers`. This should be null or a single reducer. You can use combineReducers to combine all your extra reducers into a single object.

### WebSocketHandler
In most cases you can simply initialize the WebSocketHandler with your redux store, then subscribe to all.

```javascript
import {WebSocketHandler, store} from 'channels-redux';
const webSocketHandler = new WebSocketHandler(store);
webSocketHandler.subscribeAll();
```

WebSocketHandler also provides more granular subscribeNew(model_name) and subscribeExisting(model_name, ids) functions if you desire that kind of control

### actions
Actions is a module containing the redux actions for maintaining the objects in your redux state.

Most of these you probably won't use directly but you may use these:

`patchObject(model, url, updatedFields)`

This is a useful for applying partial updates to an object. It updates the client side state and dispatches a request to update the server side state

`fetchObject(model, url)`

This is useful if you want to retrieve an object that you don't already have data for. It handles making the request and creating a new object in your redux store.

### request
request is function that returns an axios instance with the CSRFToken cookie set. This **must** be used for POST requests.

### PatientComponent

PatientComponent defines a method `isReady()`, it will not attempt to render until `isReady()` returns true. By default
if it is not ready the component renders `defaultContents()` instead. If you want this to be visible you'll need to either override
`defaultContents()` or define the css classes `.spinner-wrapper` & `.default-loading-spinner`. The following css will make
the spinners appear as a rotating blue spinner
```css
    .spinner-wrapper {
        float: inherit;
        width: 100%;
    }
    .default-loading-spinner {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        margin: 5% auto;
        animation: spin 2s cubic-bezier(0.68, -0.55, 0.27, 1.55) infinite;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
```
